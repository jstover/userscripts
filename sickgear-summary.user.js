// ==UserScript==
// @name         Sickgear Summary
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/sickgear-summary.user.js
// @version      0.1
// @description  Add summary to sickgear home
// @author       JS
// @match        http://localhost:8081/view-shows/
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let totalShows = 0;
    let totalEps = 0;
    let completeShows = 0;
    let completeEps = 0;

    const header = document.querySelector('h1.header');

    document.querySelectorAll('.progressbarText').forEach(e => {
        var values = e.innerHTML.split('/').map(x => parseInt(x, 10));
        totalShows++;
        totalEps += values[1];
        completeEps += values[0];
        if (values[0] === values[1]) { completeShows++; }
    });

    const summary = document.createElement('div');
    summary.innerHTML = `<b>Shows</b> : <i>${completeShows} / ${totalShows} (${totalShows - completeShows} incomplete)</i><br>`;
    summary.innerHTML += `<b>Episodes</b> : <i>${completeEps} / ${totalEps} (${totalEps - completeEps} missing)</i>`;
    summary.style.position = 'absolute';
    summary.style.marginTop = '1px';
    summary.style.fontSize = 'small'
    header.parentNode.insertBefore(summary, header.nextSibling)

})();
