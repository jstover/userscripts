// ==UserScript==
// @name         SE Cleaner
// @version      0.30
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/se-cleaner.user.js
// @icon         https://hg.sr.ht/~jmstover/userscripts/raw/icons/se.png
// @description  Cleanup SE sites
// @author       JS
// @match        http*://*.stackexchange.com/*
// @match        http*://*.stackoverflow.com/*
// @match        http*://askubuntu.com/*
// @match        http*://serverfault.com/*
// @match        http*://superuser.com/*
// @match        http*://stackapps.com/*
// @grant        none
// ==/UserScript==

var USER_COMMENT_BLACKLIST = [
    'Lightness Races with Monica',
    'Solar Mike',
    'David Richerby',
    'Valorum',
    'Fattie',
    'computercarguy',
    'einpoklum',
    'John B',
    'uhoh',
    'A. I. Breveleri',
    'jamesqf',
    'gerrit',
    'Tinkeringbell',
    'Andy aka',
    'm4r35n357',
    'Charlie Parker',
    'JE_Muc',
    'CJ Dennis',
    'gnicko',
    'FuzzyChef'
];

var ELEMENT_BLACKLIST = [
    "#js-gdpr-consent-banner",
    ".hero-container",
    ".old-hero",
    ".hero-box",
    ".left-sidebar",
    "#post-form",
    ".js-dismissable-hero",
    ".show-more.js-show-more",
    ".js-post-notice",
    "#announcement-banner",
    // Analytics consent
    ".js-consent-banner",
    // Job ads
    "#hireme",
    // "Add Comment" links
    "a.js-add-link.comments-link",
    // "Improve this question" links
    "a.js-suggest-edit-post",
    // "Follow" links
    "button.js-follow-post",
    // "Not the answer you're looking for?" box
    "h2.bottom-notice",
    // Overflow Blog / Meta featured box
    "#sidebar > .s-sidebarwidget",
];

/* ----- Compatability functions ----- */

function hasClass(element, className) {
    return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
}

function findAncestor (el, cls) {
    while ((el = el.parentElement) && !hasClass(el, cls)){/* empty loop */};
    return el;
}

function removeElement(el){
    if (el !== null){
        el.parentNode.removeChild(el);
    }
}

HTMLElement.prototype.removeClass = function(remove) {
  var newClassName = "";
  var i;
  var classes = this.className.split(" ");
  for (i = 0; i < classes.length; i++) {
    if (classes[i] !== remove) {
      newClassName += classes[i] + " ";
    }
  }
  if (newClassName.trim) {
    newClassName = newClassName.trim();
  }
  this.className = newClassName;
}

/* ----------------------------------- */


/* Remove comments from a given user */
function removeUserComments(username){
    var comments = document.querySelectorAll(".comment > .comment-text > .comment-body a.comment-user");
    var removed = 0;
    Array.prototype.forEach.call(comments, function(el, i){
        if (el.text === username || el.text === `${username}♦`){
            removeElement(findAncestor(el, "comment"));
            removed++;
        }
    });
    return removed;
}

/* Remove all comments from users in USER_COMMENT_BLACKLIST */
function removeBlacklistComments(){
    Array.prototype.forEach.call(USER_COMMENT_BLACKLIST, function(username){
        var removed = removeUserComments(username);
        if (removed > 0){
            console.log("Removed " + removed + " comments by " + username);
        }
    });
}

/* Remove all elements in ELEMENT_BLACKLIST */
function removeBlacklistElements(){
    Array.prototype.forEach.call(ELEMENT_BLACKLIST, function(selector, i){
        var elements = document.querySelectorAll(selector);
        Array.prototype.forEach.call(elements, function(el, i){
            removeElement(el);
        });
        console.log("Removed elements matching \"" + selector + "\": " + elements.length);
    });
}

/* Expand Hot Network Questions sidebar */
function expandHotNetworkQuestions(){
    var hidden = document.querySelectorAll("li.js-hidden");
    Array.prototype.forEach.call(hidden, function(el, i){
        el.removeClass("dno");
        el.removeClass("js-hidden");
        el.style.display = '';
    });
}

/* Expand all answer comments by clicking the 'show x more comments' link */
function expandComments(){
    document.querySelectorAll(".js-show-link.comments-link:not(.dno)").forEach(e=>{e.click();});
}

/* Remove '-gps-track' attributes/classes from links */
function removeGPSTracking(){
    document.querySelectorAll("a").forEach(e=>{
        e.removeClass("js-gps-track");
        e.removeAttribute("data-gps-track");
    });
}

/* remove the fade-out effect of downvoted answers */
function showDownvotedAnswers(){
    document.querySelectorAll(".downvoted-answer").forEach((e)=>{
        e.removeClass("downvoted-answer");
    });
}


/* Revert the shitty font selection change */
function resetFont(){
  document.head.appendChild(
    Object.assign(
      document.createElement('style'), {
        textContent: `
          body {
            --ff-sans: Arial, "Helvetica Neue", Helvetica, sans-serif;
            --ff-mono:Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, sans-serif;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
          }
          code, .s-prose code, pre.s-code-block, textarea.wmd-input, textarea#wmd-input, .full-diff .content {
            font-family: var(--ff-mono);
          }
          .top-bar, .topbar-dialog, #user-menu, .topbar-dialog .header h3 a, .topbar-dialog .pinned-site-editor-container .remove-pinned-site-link a, .topbar-dialog .modal-content .message-text h4 {
            font-family: var(--ff-sans) !important;
          }`
      }
    )
  );
}

/* Remove the top bar altogether */
function removeTopBarHeader(){
  const el = document.querySelector("header.top-bar");
  document.body.removeChild(el);
  document.body.style.paddingTop = '0px';
}



(function() {
    'use strict';

    resetFont();

    // Remove elements in ELEMENT_BLACKLIST
    removeBlacklistElements();

    // Remove comments from users in USER_COMMENT_BLACKLIST on a timer to apply when expanded
    setInterval(removeBlacklistComments, 1000);

    // Expand the Hot Network Questions sidebar
    expandHotNetworkQuestions();
    
    // Expand the comments
    setTimeout(expandComments, 1000);

    // Remove fade on downvoted answers
    showDownvotedAnswers();

    // Remove '-gps-track' attributes/classes from links
    removeGPSTracking();
  
    // Remove the login/signup top bar
    removeTopBarHeader();

})();

