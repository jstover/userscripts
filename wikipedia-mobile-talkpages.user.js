// ==UserScript==
// @name         Wikipedia - Enable 'talk' link on mobile pages
// @version      0.01
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/wikipedia-mobile-talkpages.user.js
// @description  Add a link to the Talk page in mobile view.
// @author       JS
// @match        http*://*.m.wikipedia.org/wiki/*
// @grant        none
// ==/UserScript==

(() => {
    'use strict';

    const refElement = document.getElementById('page-actions-watch');
    const talkLink = document.createElement('li');
    talkLink.innerHTML = '<a href="TALKPAGE">Talk</a>';
    refElement.parentNode.insertBefore(talkLink, refElement);
    

})();
