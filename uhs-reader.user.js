// ==UserScript==
// @name         UHS Reader
// @namespace    https://git.sr.ht/~jmstover/userscripts
// @downloadURL  https://git.sr.ht/~jmstover/userscripts/blob/master/uhs-reader.user.js
// @version      0.2
// @description  Make Ultimate Hint System web interface usable
// @author       JS
// @match        http://www.uhs-hints.com/uhsweb/hints/*
// @grant        none
// ==/UserScript==

function removeElement(el) {
    if (el){
        el.parentNode.removeChild(el);
    }
}

function fix_style(){
    console.log("UHSS: Links");
    var links = document.querySelectorAll(".uhsweb-list");
    for(var i=0; i<links.length; i++){
        links[i].style.fontSize = "x-large";
        links[i].style.lineHeight = "150%";
    }
    document.querySelector("#content").style.fontSize = "x-large";

    console.log("UHSV: full width");
    document.querySelector("#page-wrapper").style.width = "auto";
    document.querySelector("#content").style.width = "auto";

    console.log("USHV: Sidebars / Infobox");
    removeElement(document.querySelector(".sidebar-300"))
    removeElement(document.querySelector(".sidebar"))
    removeElement(document.querySelector("#infobox"));

    console.log("USHV: Title");
    document.querySelector("#content > p").style.fontSize = "100%";

    console.log("USHV: more / up level buttons");
    var more = document.querySelector("#morehints");
    if (more){ more.style.fontSize = "medium"; }
    document.querySelectorAll("#uponelevelbox td").forEach(function(e){e.style.fontSize = "medium";});
    document.querySelector("#uponelevelbox").style.marginTop = "60px";

}


(function() {
    'use strict';
    fix_style();
})();
