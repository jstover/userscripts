// ==UserScript==
// @name         Library Genesis DOI Linker
// @version      0.6
// @namespace    https://git.sr.ht/~jmstover/userscripts
// @downloadURL  https://git.sr.ht/~jmstover/userscripts/blob/master/libgen-doi-linker.user.js
// @description  Convert all DOI links to Library Genesis searches
// @author       JS
// @match        http://*/*
// @match        https://*/*
// @grant        none
// @noframes
// ==/UserScript==


// Replace DOI URLs with a link to here
REDIRECT_URL = 'http://gen.lib.rus.ec/scimag/index.php?s=';


// URL patterns to match for replacement
DOI_URLS = [
    'http://doi.org/',
    'https://doi.org/',
];


// List of DOM elements to try looking at for a DOI URL
DOI_ELEMENTS = [
    '#doi-url',
    'another'
];


// Take a DOI URL and convert it to a `REDIRECT_URL` URL
function replace_url(doi_url){
    if (doi_url.match("^"+REDIRECT_URL)){
        console.log("MATCH");
        return doi_url;
    }

    url = doi_url;
    for (var i=0; i<DOI_URLS.length; i++){
        url = url.replace(DOI_URLS[i], REDIRECT_URL);
    }
    return url;
};


function extrct_doi(url){
    doi = url;
    for (var i=0; i<DOI_URLS.length; i++){
        url = url.replace(DOI_URLS[i], '');
    }
    return doi;
};

window.addEventListener ("load", main, false);

function main () {
    for (var i=0; i<DOI_ELEMENTS.length; i++){
        urls = $(DOI_ELEMENTS[i]);

        if (urls.length > 0){
            console.log("Found " + urls.length + " DOI URL's");

            urls.each(function(i){
                new_url = replace_url($(this).html());
                console.log(new_url);

                $(this).html("<a href=\"" + new_url + "\">" + $(this).html() + "</a>");
            });

        }

    }
}

