// ==UserScript==
// @name         Autoclicker: 1Link.club
// @version      0.1
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/autoclick-1Link.user.js
// @description  Autoclick for 1link.club
// @author       JS
// @match        http*://1link.club/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const clicked = {
        next: false,
        download: false
    };

    function tryClick(id, name) {
        if (clicked[name]) { return; }
        const el = document.querySelector(id);
        if (el !== null) {
            el.click();
            clicked[name] = true;
        }
    };

    setInterval(() => {
        tryClick('#go_next');
        tryClick('#download');
    }, 1000);
})();
