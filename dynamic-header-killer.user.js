// ==UserScript==
// @name         Dynamic Header Killer
// @version      0.02
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/dynamic-header-killer.user.js
// @description  I fucking hate dynamic headers
// @author       JS
// @match        http*://www.jbhifi.com.au/*
// @match        http*://www.bigw.com.au/*
// @grant        none
// ==/UserScript==


function setElementStyle(selector, property, value){
  document.querySelectorAll(selector).forEach(e => {
    e.style[property] = value;
  });
}


(function() {
    'use strict';
  
    // bigw
    setElementStyle('header#header', 'position', 'absolute');
  
    // Shopify sites like jbhifi
    setElementStyle('#shopify-section-header', 'transform', 'none');
    setElementStyle('#shopify-section-header', 'position', 'absolute');

})();

