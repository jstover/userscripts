// ==UserScript==
// @name         Autoclicker: CutDL.xyz
// @version      0.1
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/autoclick-CutDL.user.js
// @description  Autoclicker/Link Revealer for CutDL.xyz
// @author       JS
// @match        https://cutdl.xyz/*
// @grant        none
// ==/UserScript==

function selectText(node) {
    if (document.body.createTextRange) {
        const range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        const selection = window.getSelection();
        const range = document.createRange();
        range.selectNodeContents(node);
        selection.removeAllRanges();
        selection.addRange(range);
    } else {
        console.warn("Could not select text in node: Unsupported browser.");
    }
}

(function() {
    'use strict';

    let linkFound = false;

    setInterval(() => {

        if (linkFound) { return; }

        const linkButton = document.querySelector('.get-link');
        if (linkButton === null) { return; }

        if (!linkButton.href.startsWith('javascript:')) {
            const linkBox = document.createElement('pre');
            linkBox.style.marginTop = '10px';
            linkBox.innerText = linkButton.href;
            linkFound = true;
            linkButton.parentNode.insertBefore(linkBox, linkButton.nextSibling);
            selectText(linkBox);
        }

    }, 1000);

})();
