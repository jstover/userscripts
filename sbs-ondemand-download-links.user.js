// ==UserScript==
// @name         SBS OnDemand: Download Links
// @version      0.13
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/sbs-ondemand-download-links.user.js
// @icon         https://hg.sr.ht/~jmstover/userscripts/raw/icons/sbs.png
// @description  Show download links for SBS ondemand videos
// @author       JS
// @match        https://www.sbs.com.au/ondemand/*
// @grant        none
// ==/UserScript==


const LINK_SELECTOR = 'div[role="tabpanel"] a[href^="/ondemand"]'


function waitForLinks(){
  return new Promise(resolve => {
    const timer = setInterval(function(){
      if (document.querySelectorAll(LINK_SELECTOR).length > 0){
        clearInterval(timer);
        resolve();
      }
    }, 200);
  });
}

function getCurrentShowName(){
  const showHeader = document.querySelector("div#root h1");
  return showHeader === null ? '%(title)s' : showHeader.textContent.replaceAll(" ", ".");
}

function getCurrentSeason(){
  const seasonButton = document.querySelector('div[aria-label="Season tabs"] a[aria-selected=true]');
  if (seasonButton === null)
    return "00";
  const seasonText = seasonButton.textContent.split(" ");
  return (seasonText[seasonText.length - 1] || "00").padStart(2, "0");
}

function getEpisode(e){
  if (e === null)
    return "00";
  const text = e.textContent.split(". ", 2);
  const epnum = text[0].padStart(2, "0");
  const eptitle = (text[1] || "").trim().replaceAll(" ", ".");
  return eptitle === "" ? epnum : `${epnum}.${eptitle}`;
}


function createCopyButton(target){

  const copyText = document.createElement('span');
  copyText.style.paddingLeft = '10px';
  copyText.style.color = '#fdb717';
  copyText.style.fontWeight = 'bold';

  const btn = document.createElement('button');
  btn.innerText = 'Copy to Clipboard';
  btn.onclick = function(){
    navigator.clipboard.writeText(target.innerText).then(function() {
      copyText.innerText = 'Copied!';
    }, function(err) {
      copyText.innerText = err.toString();
    });
  };

  const btnWrapper = document.createElement('div');
  btnWrapper.insertBefore(btn, null);
  btnWrapper.insertBefore(copyText, null);
  return btnWrapper;
}

const replaceInvalidChars = s => "#:/\\".split('').reduce((agg, curr) => agg.replace(curr, ""), s)


function createLinkElements(){
  const show = getCurrentShowName();
  const season = getCurrentSeason();
  const allLinks = [];

  document.querySelectorAll(LINK_SELECTOR).forEach(e => {
    const href = e.href;
    const newLink = document.createElement('pre');

    const episodeHeader = e.parentNode.parentNode.querySelector("h2,h3");
    const episode = getEpisode(episodeHeader);

    const filename = replaceInvalidChars(`${show}.S${season}E${episode}.WEB-DL.%(height)sp-SBS.%(ext)s`);
    newLink.innerText = `yt-dlp -o"${filename}" '${href}'`;
    allLinks.push(newLink.innerText);
    newLink.style.margin = '0px';
    newLink.style.color = '#f5deb3';

    const copyButton = createCopyButton(newLink);
    const panel = document.createElement('div');
    panel.insertBefore(newLink, null);
    panel.insertBefore(copyButton, null);

    episodeHeader.parentNode.insertBefore(panel, null);
  });

  // Bulk command panel
  const episodeList = document.querySelector("[role=tabpanel] > div");
  if (episodeList !== null){

    const bulkCmd = document.createElement('pre');
    bulkCmd.textContent = allLinks.join("\n");

    const bulkPanel = document.createElement('div');
    bulkPanel.classList = episodeList.firstChild.classList;
    bulkPanel.style.flexDirection = 'column';
    bulkPanel.insertBefore(bulkCmd, null);
    bulkPanel.insertBefore(createCopyButton(bulkCmd), null);
    episodeList.insertBefore(bulkPanel, null);
  }
}


(function() {
  'use strict';

  waitForLinks().then(() => {

    // Create links for the season which loaded
    createLinkElements();

    // Update links when season changes
    document.querySelectorAll('div[role="tablist"] button').forEach(e => {
      e.addEventListener('click', function(e){
        let target = e.target;
        if (target.nodeName && target.nodeName.toLowerCase() == 'span'){
          target = target.parentNode;
        }
        if (target.getAttribute('aria-selected') === 'false') {
          setTimeout(function(){
            waitForLinks().then(createLinkElements())
          }, 0);
        }
      });
    });

  });

})();

