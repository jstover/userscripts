// ==UserScript==
// @name         ABC iview: Download Links
// @version      0.05
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/iview-download-links.user.js
// @icon         https://hg.sr.ht/~jmstover/userscripts/raw/icons/abc-iview.png
// @description  Show youtube-dl compatible links on ABC iview show pages
// @author       JS
// @match        https://iview.abc.net.au/show/*
// @grant        none
// ==/UserScript==

const episodeSelector = "header + section a[aria-label='Share by Copy link']";

const youtubeDl = `youtube-dl -fbest -o'%(title)s.WEB-DL.%(height)sp-ABC.%(ext)s'`;

/* ========== Utils ========== */
function parentNode(element, depth) {
    let parent = element.parentNode;
    for (let i=0; i<depth-1 ;i++) {
        parent = parent.parentNode;
    }
    return parent;
}

function longestCommonPrefix(array){
    var A = array.concat().sort()
    var a1= A[0]
    var a2= A[A.length-1]
    let i=0;
    while (i < A[0].length && a1.charAt(i) === a2.charAt(i)) { i++; }
    return a1.substring(0, i);
}

function findShowInfo(){
    let seriesHeader = document.querySelector('#seriesSelector h2');
    if (seriesHeader === null) {
        seriesHeader = document.querySelector('header > h2');
    }
    const series = seriesHeader.innerText;
    const episodes = Array.from(document.querySelectorAll(episodeSelector))
        .map(e => {
            e.click();
            const urlElement = parentNode(e, 3).nextSibling;
            const url = (urlElement === null) ? null : urlElement.firstChild.innerText;
            const name = parentNode(e, 11).querySelector('h3').innerText;
            return {url, name}
        })
        .filter(e => e !== null);
    return {series, episodes};
}

function bulkDownloadCommand(episodes) {
    const urls = episodes.map(ep => ep.url);
    if (urls.length == 1)
        return `${youtubeDl} "${urls[0]}"`
    const prefix = longestCommonPrefix(urls);
    const prefixRE = new RegExp(`^${prefix}`);
    const shortUrls = urls.map(url => url.replace(prefixRE, ''));
    return `for url in ${shortUrls.join(' ')}; do
    ${youtubeDl} "${prefix}$url"
done`;
}


/* ========== Make Elements ========== */
function createButton(text, onclick){
    const wrapper = document.createElement('div');
    const button = document.createElement('button');
    button.onclick = onclick;
    button.textContent = text;
    wrapper.insertBefore(button, null);
    return wrapper;
}

function createLinkPanel(){
    const showHeader = document.querySelector("article[data-component='ShowHeader']");
    console.log(showHeader);

    const firstSection = showHeader.nextSibling.querySelector('section');
    const panel = document.createElement('div');
    panel.id = 'DownloadLinkPanel';
    panel.classList = firstSection.classList;
    panel.style.paddingTop = '10px';
    panel.style.paddingBottom = '10px';
    panel.style.flexDirection = 'column';

    const result = document.createElement('pre');
    result.style.display = 'none';

    // Button handler looks at what is currently visible on the page
    const button = createButton('Show Download Links', () => {
        const show = findShowInfo();
        let txt = '';
        show.episodes.forEach(ep => {
            txt += `\n# ${show.series} - ${ep.name}\n${ep.url}\n`;
        });
        txt += `\n# ${show.series} - All \n${bulkDownloadCommand(show.episodes)}`

        result.innerText = txt;
        result.style.display = 'unset';
    });

    panel.insertBefore(button, null);
    panel.insertBefore(result, null);
    firstSection.parentNode.insertBefore(panel, firstSection);
    return {container: panel, button: button, result: result};
}

function waitForEpisodeList(){
    return new Promise((resolve, reject) => {
        const timer = setInterval(function(){
            const episodes = document.querySelectorAll(episodeSelector);
            console.log(episodes);
            if (episodes.length > 0) {
                clearInterval(timer);
                resolve();
            }
        }, 200);
    });
}


(() => {

    waitForEpisodeList().then(() => {
        const panel = createLinkPanel();
    });

})();

