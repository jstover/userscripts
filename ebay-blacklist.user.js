// ==UserScript==
// @name         Ebay - Hide blacklisted sellers
// @version      0.5
// @namespace    https://git.sr.ht/~jmstover/userscripts
// @downloadURL  https://git.sr.ht/~jmstover/userscripts/blob/master/ebay-blacklist.user.js
// @description  Hide sellers found in SELLER_BLACKLIST from ebay search results
// @author       JS
// @include      /^https://www\.ebay\.com/sch/i\.html.*$/
// @include      /^https://www\.ebay\.(..)/sch/i\.html.*$/
// @include      /^https://www\.ebay\.com?\.(..)/sch/i\.html.*$/
// @grant        none
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @license      MIT
// ==/UserScript==

var $ = window.jQuery;

var SELLER_BLACKLIST = [
];

(function() {
    'use strict';

    for (var i=0; i<SELLER_BLACKLIST.length; i++){
        var seller = SELLER_BLACKLIST[i];
        var removed = $("#ResultSetItems .sresult .lvdetails > li:contains(Seller: " + seller + ")").parents(".sresult").remove();
        if (removed.length > 0){
            console.log("Removed " + removed.length + " items from blacklisted seller: " + seller);
        }
    }

})();


