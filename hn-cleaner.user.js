// ==UserScript==
// @name         HN Cleaner
// @version      0.04
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://hg.sr.ht/~jmstover/userscripts/raw/hn-cleaner.user.js
// @icon         https://hg.sr.ht/~jmstover/userscripts/raw/icons/hackernews.png
// @description  Cleanup HN
// @author       JS
// @match        http*://news.ycombinator.com/item?*
// @grant        none
// ==/UserScript==

const TITLE_COLOUR = '#000000';

const TEXT_COLOUR = '#101010';

const LINKBOX_COLOUR = '#FFDC8A';

const LINK_COLOUR = '#0000C0';


function findLinkComment(linkElement){
    let parent = linkElement.parentNode;
    while (parent !== null && !parent.classList.contains('comment')) {
        parent = parent.parentNode;
    }
    return parent;
}


(() => {
    'use strict';

    // Stop greying out downvoted comments
    document.querySelectorAll('span.commtext').forEach(e => {
        e.className = 'commtext c00';
    });

    // Don't use grey for text colour
    console.log(document.querySelector('body'));
    document.querySelector('body').style.color = TEXT_COLOUR;
    document.querySelectorAll('table.fatitem td').forEach(e => {
        e.style.color = TEXT_COLOUR;
    });

    // Bold Title
    document.querySelectorAll('td.title > a').forEach(e => {
        e.style.fontWeight = 'bold';
        e.style.color = TITLE_COLOUR;
    });

    // Add link buttons
    document.querySelectorAll('div.comment a:not([href^="reply"])').forEach(e => {
        e.style.color = LINK_COLOUR;
        const commentElement = findLinkComment(e);
        const replyLink = commentElement.querySelector('.reply');
        const link = document.createElement('a');
        link.style.maxWidth = 'unset';
        link.style.width = '100%';
        link.href = e.toString();
        link.innerText = e.toString();
        const linkBox = document.createElement('div');
        linkBox.style.marginTop = '5px';
        linkBox.style.background = LINKBOX_COLOUR;
        linkBox.style.padding = '5px';
        linkBox.insertBefore(link, null);
        //linkBox.innerHTML = `<a href="${e.innerText}">${e.innerText}</a>`;
        replyLink.parentNode.insertBefore(linkBox, replyLink);
    })

})();

