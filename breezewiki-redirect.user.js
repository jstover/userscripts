// ==UserScript==
// @name         Breezewiki Redirect
// @version      0.1
// @namespace    https://hg.sr.ht/~jmstover/userscripts
// @downloadURL  https://codeberg.org/jstover/userscripts/raw/branch/master/breezewiki-redirect.user.js
// @description  Redirect fandom.com to configured Breezewiki instance
// @author       JS
// @match        https://*.fandom.com/*
// @grant        none
// @run-at       document-start
// ==/UserScript==

const INSTANCE = "http://localhost:10416"

window.location.replace(`${INSTANCE}/${window.location.host.split(".")[0]}${window.location.pathname}`);

