// ==UserScript==
// @name        YouTube continue playing
// @version     1.0
// @namespace   Violentmonkey Script
// @downloadURL https://hg.sr.ht/~jmstover/userscripts/raw/sbs-ondemand-download-links.user.js
// @icon        https://hg.sr.ht/~jmstover/userscripts/raw/icons/youtube.png
// @description Automatically dismiss the "Continue watching?" dialog on YouTube
// @author      JS
// @match       https://www.youtube.com/watch
// @grant       none
// ==/UserScript==


function continue_playing(){
  const button = document.querySelector('.yt-confirm-dialog-renderer #confirm-button');
  if (button === null) { return; }
  button.click();
}

(() => {
  setInterval(continue_playing, 2500);
})();

