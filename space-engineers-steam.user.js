// ==UserScript==
// @name         Space Engineers Steam Mod Helper
// @version      0.4.5
// @namespace    https://git.sr.ht/~jmstover/userscripts
// @downloadURL  https://git.sr.ht/~jmstover/userscripts/blob/master/space-engineers-steam.user.js
// @description  Script to help with adding mods to a dedicated server from the steam workshop.
// @author       JS
// @match        https://steamcommunity.com/app/244850/workshop/
// @match        https://steamcommunity.com/workshop/browse/?appid=244850&*
// @match        https://steamcommunity.com/workshop/filedetails*
// @match        https://steamcommunity.com/sharedfiles/filedetails/?*
// @grant        none
// ==/UserScript==
//

var BUTTON_LONG_LEXT = 'Server Config XML';

var MODAL_ID = 'space_engineers_helper_modal';


function setVendorPrefixStyle(el, property, value){
    el.style['webkit' + property] = value;
    el.style['moz' + property] = value;
    el.style['ms' + property] = value;
    el.style['o' + property] = value;
}


function findModTitle(){
    // Try find title in the known places
    
    var e = document.querySelector('.workshopItemTitle');
    if (e) { return e.innerText; }


    return 'UNKNOWN';
}


function findModAppid(){
    // Try find appid in the known places

    var url = new URL(window.location);
    var appid = url.searchParams.get('id');
    if (appid) { return appid; }


    return '0';
}

function hideModal(){
    var modal = document.querySelector('#' + MODAL_ID);
    if (modal){
        modal.style.display = 'none';
    }
}

function createModal(){
    var modal = document.createElement('div');
    modal.id = MODAL_ID;
    modal.style.position = 'fixed';
    modal.style.display = 'none';
    modal.style.zIndex = 999;
    modal.style.left = 0;
    modal.style.top = 0;
    modal.style.width = '100%';
    modal.style.height = '100%';
    modal.style.overflow = 'auto';
    modal.style.backgroundColor = 'rgba(0, 0, 0, 0.85)';
    modal.onmousedown = function(e){
        if (e.target == modal){
            hideModal();
        }
    };
    
    var modalContent = document.createElement('div');
    modalContent.style.top = '50%';
    modalContent.style.left = '50%';
    modalContent.style.position = 'absolute';
    modalContent.style.transform = 'translateX(-50%) translateY(-50%)';
    modalContent.style.backgroundColor = '#FFF';
    modalContent.style.textAlign = 'right';
    modalContent.style.padding = '2px';

    var modalClose = document.createElement('span');
    modalClose.innerText = '[x]';
    modalClose.style.cursor = 'pointer';
    setVendorPrefixStyle(modalClose, 'UserSelect', 'none');
    modalClose.onclick = hideModal;

    var modalText = document.createElement('pre');
    modalText.style.margin = '0px 2.3em 14px 10px';
    modalText.style.fontSize = '1.5em';
    modalText.style.textAlign = 'left';
    modalText.style.color = '#000';

    var modTitle = findModTitle();
    var modAppid = findModAppid();
    modalText.innerText = `    <ModItem FriendlyName="${modTitle}">
      <Name>${modAppid}.sbm</Name>
      <PublishedFileId>${modAppid}</PublishedFileId>
    </ModItem>`;
    
    modalContent.append(modalClose);
    modalContent.append(modalText);
    modal.append(modalContent);
    
    document.onkeydown = function(e){
        if (e.key == 'Escape'){
            hideModal()
        }
    };

    document.querySelector('body').append(modal);
}


function makeButton(cls, text){
    var button = document.createElement('div');
    button.classList.add(cls);
    button.innerHTML = text;
    button.style.backgroundColor = '#FFB62F';
    button.style.color = '#000';
    button.style.borderRadius = '0.5em';
    button.style.padding = '5px';
    button.style.textAlign = 'center';
    button.style.cursor = 'pointer';
    button.onclick = function(){
        document.querySelector('#' + MODAL_ID).style.display = 'block';
    };
    return button;
}


function makeHeaderSeparator(){
    var e = document.createElement('div');
    e.style.marginBottom = '16px';
    e.className = 'dotted_hr_uri hr padded';
    return e;
}


function appendModHeaderButton(){
    var header = document.querySelector('.workshop_item_header > .col_right');
    if (header) {
        header.append(makeHeaderSeparator());
        header.append(makeButton('rightDetailsBlock', BUTTON_LONG_LEXT));
    }
}


(function() {
    'use strict';
   
    createModal();
    
    appendModHeaderButton();

})();
